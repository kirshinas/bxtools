<?php
/**
 * @author Alexandr Kirshin <kirshin.as@gmail.com>
 */

namespace kirshinas\helpers;

/**
 * Class Session
 * @package Victory\Helpers
 */
class SessionHelper
{

    /**
     * Добавить сообщение
     * @param $code
     * @param $value
     */
    public static function setFlash($code, $value)
    {
        $_SESSION['flash'][$code] = $value;
    }

    /**
     * Проверить наличие сообщения
     * @param $code
     * @return bool
     */
    public static function hasFlash($code)
    {
        return isset($_SESSION['flash'][$code]);
    }

    /**
     * Получить сообщение
     * @param $code
     * @return mixed
     */
    public static function getFlash($code)
    {
        $value = $_SESSION['flash'][$code];
        unset($_SESSION['flash'][$code]);

        return $value;
    }

} 