<?php
/**
 * @author Alexandr Kirshin <kirshin.as@gmail.com>
 */

namespace kirshinas\helpers;

use \CIBlockElement;

class IBlockHelper
{

    /**
     * Установка значения пользовательского поля
     * @param string $entity_id Имя объекта
     * @param string $value_id Идентификатор элемента
     * @param string $uf_id Имя пользовательского свойства
     * @param string $uf_value Значение, которое сохраняем
     *
     * @return string
     */
    public static function setUserField($entity_id, $value_id, $uf_id, $uf_value)
    {
        return $GLOBALS['USER_FIELD_MANAGER']->Update($entity_id, $value_id, Array($uf_id => $uf_value));
    }

    /**
     * Получить значение пользовательского поля
     * @param string $entity_id Имя объекта
     * @param int $value_id Идентификатор элемента
     * @param string $uf_id Имя пользовательского свойства
     *
     * @return string Значение поля
     */
    public static function getUserField($entity_id, $value_id, $uf_id)
    {
        $arUF = $GLOBALS['USER_FIELD_MANAGER']->GetUserFields($entity_id, $value_id);

        return $arUF[$uf_id]['VALUE'];
    }


    /**
     * Получить значение пользовательского поля для раздела инфоблока
     * @param int $iblock_id Ид инфоблока
     * @param int $value_id Идентификатор элемента
     * @param string $uf_id Имя пользовательского свойства
     *
     * @return string Значение поля
     */
    public static function getUserFieldSection($iblock_id, $value_id, $uf_id)
    {
        return self::getUserField('IBLOCK_' . $iblock_id . '_SECTION', $value_id, $uf_id);
    }

    /**
     * Получить элемент по его Ид
     * @param $id Ид элемента
     * @return bool|array Элемента
     */
    public static function getIBlockElement($id)
    {
        $resElement = CIBlockElement::GetList(array(), array('ID' => $id));
        if ($obElement = $resElement->GetNextElement()) {
            $arElement = $obElement->GetFields();
            $arElement['PROPERTIES'] = $obElement->GetProperties();

            return $arElement;
        }

        return false;
    }
}