<?php
/**
 * @author Alexandr Kirshin <kirshin.as@gmail.com>
 */

namespace kirshinas\helpers;

class FormatHelper
{

    /**
     * Форматирование размера файла
     * @param $file
     * @param int $precision
     * @return string
     */
    public static function  asBytes($file, $precision = 2)
    {
        $bytes = @filesize($_SERVER['DOCUMENT_ROOT'] . $file);
        $units = array('b', 'kb', 'mb', 'gb', 'tb');

        $bytes = max($bytes, 0);
        $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
        $pow = min($pow, count($units) - 1);

        $bytes /= pow(1024, $pow);

        return round($bytes, $precision) . ' ' . $units[$pow];
    }

    /**
     * @param $value
     * @param int $precision
     * @param int $mode
     * @return string
     */
    public static function  asMoney($value, $precision = 2, $mode = 0)
    {

        $units = array('', 'тыс', 'млн', 'трл');

        $value = max($value, 0);
        $pow = floor(($value ? log($value) : 0) / log(1000));
        $pow = min($pow, count($units) - 1);

        $value /= pow(1000, $pow);
        switch ($mode) {
            case 0:
                return number_format($value, $precision, ',', ' ') . ' ' . $units[$pow];
            case 1:
                return number_format($value, $precision, ',', ' ');
            case 2:
                return
                    $units[$pow];
        }
    }

    /**
     * Форматирование множественной формы
     * @param $n
     * @param string $f1
     * @param string $f2
     * @param string $f3
     * @return string
     */
    public static function plural($n, $f1 = 'сообщение', $f2 = 'сообщения', $f3 = 'сообщений')
    {
        return $n % 10 == 1 && $n % 100 != 11 ? $f1 : ($n % 10 >= 2 && $n % 10 <= 4 && ($n % 100 < 10 || $n % 100 >= 20) ? $f2 : $f3);
    }

    /**
     * Транслитерация текста
     * @param $text
     * @return string
     */
    public static function translit($text)
    {
        $tr = array(
            "А" => "a",
            "Б" => "b",
            "В" => "v",
            "Г" => "g",
            "Д" => "d",
            "Е" => "e",
            "Ж" => "j",
            "З" => "z",
            "И" => "i",
            "Й" => "y",
            "К" => "k",
            "Л" => "l",
            "М" => "m",
            "Н" => "n",
            "О" => "o",
            "П" => "p",
            "Р" => "r",
            "С" => "s",
            "Т" => "t",
            "У" => "u",
            "Ф" => "f",
            "Х" => "h",
            "Ц" => "ts",
            "Ч" => "ch",
            "Ш" => "sh",
            "Щ" => "sch",
            "Ъ" => "",
            "Ы" => "yi",
            "Ь" => "",
            "Э" => "e",
            "Ю" => "yu",
            "Я" => "ya",
            "а" => "a",
            "б" => "b",
            "в" => "v",
            "г" => "g",
            "д" => "d",
            "е" => "e",
            "ж" => "j",
            "з" => "z",
            "и" => "i",
            "й" => "y",
            "к" => "k",
            "л" => "l",
            "м" => "m",
            "н" => "n",
            "о" => "o",
            "п" => "p",
            "р" => "r",
            "с" => "s",
            "т" => "t",
            "у" => "u",
            "ф" => "f",
            "х" => "h",
            "ц" => "ts",
            "ч" => "ch",
            "ш" => "sh",
            "щ" => "sch",
            "ъ" => "y",
            "ы" => "yi",
            "ь" => "",
            "э" => "e",
            "ю" => "yu",
            "я" => "ya",
            " " => "_",
            "-" => "_",
            "." => "",
            "/" => "_"
        );
        $urlstr = strtr($text, $tr);

        $urlstr = preg_replace('/[^A-Za-z0-9_\-]/', '', $urlstr);

        return strtolower($urlstr);
    }

    public static function asDuration($duration)
    {
        $part = explode(':', $duration);
        $result = '';
        if ($h = intval($part[0])) {
            $result .= $h . ' ' . self::plural($h, 'ч.', 'ч.', 'ч.') . ' ';
        }
        if (isset($part[1]) && $m = intval($part[1])) {
            $result .= $m . ' ' . self::plural($m, 'мин.', 'мин.', 'мин.');
        }

        return $result;
    }

    public static function asDayOfWeek($w)
    {
        $days = array(
            'пн',
            'вт',
            'ср',
            'чт',
            'пт',
            'сб',
            'вс'
        );

        return $days[intval($w) - 1];
    }

    public static function asDayOfWeekFull($w)
    {
        $days = array(
            'Понедельник',
            'Вторник',
            'Среда',
            'Четверг',
            'Понедельник',
            'Суббота',
            'Воскресенье'
        );

        return $days[intval($w) - 1];
    }

    public static function asMonth($m)
    {

        $days = array(
            'Январь',
            'Февраль',
            'Март',
            'Апрель',
            'Май',
            'Июнь',
            'Июль',
            'Август',
            'Сентябрь',
            'Октябрь',
            'Ноябрь',
            'Декабрь',
        );

        return $days[intval($m) - 1];
    }

    public static function num2str($num,$rubShow=true, $kopShow=true) {
        $nul='ноль';
        $ten=array(
            array('','один','два','три','четыре','пять','шесть','семь', 'восемь','девять'),
            array('','одна','две','три','четыре','пять','шесть','семь', 'восемь','девять'),
        );
        $a20=array('десять','одиннадцать','двенадцать','тринадцать','четырнадцать' ,'пятнадцать','шестнадцать','семнадцать','восемнадцать','девятнадцать');
        $tens=array(2=>'двадцать','тридцать','сорок','пятьдесят','шестьдесят','семьдесят' ,'восемьдесят','девяносто');
        $hundred=array('','сто','двести','триста','четыреста','пятьсот','шестьсот', 'семьсот','восемьсот','девятьсот');
        $unit=array( // Units
            array('копейка' ,'копейки' ,'копеек',	 1),
            array('рубль'   ,'рубля'   ,'рублей'    ,0),
            array('тысяча'  ,'тысячи'  ,'тысяч'     ,1),
            array('миллион' ,'миллиона','миллионов' ,0),
            array('миллиард','милиарда','миллиардов',0),
        );
        //
        list($rub,$kop) = explode('.',sprintf("%015.2f", floatval($num)));
        $out = array();
        if (intval($rub)>0) {
            foreach(str_split($rub,3) as $uk=>$v) { // by 3 symbols
                if (!intval($v)) continue;
                $uk = sizeof($unit)-$uk-1; // unit key
                $gender = $unit[$uk][3];
                list($i1,$i2,$i3) = array_map('intval',str_split($v,1));
                // mega-logic
                $out[] = $hundred[$i1]; # 1xx-9xx
                if ($i2>1) $out[]= $tens[$i2].' '.$ten[$gender][$i3]; # 20-99
                else $out[]= $i2>0 ? $a20[$i3] : $ten[$gender][$i3]; # 10-19 | 1-9
                // units without rub & kop
                if ($uk>1) $out[]= self::plural($v,$unit[$uk][0],$unit[$uk][1],$unit[$uk][2]);
            } //foreach
        }
        else $out[] = $nul;
        if($rubShow) {
            $out[] = self::plural(intval($rub), $unit[1][0], $unit[1][1], $unit[1][2]); // rub
        }
        if($kopShow) {
            $out[] = $kop . ' ' . self::plural($kop, $unit[0][0], $unit[0][1], $unit[0][2]); // kop
        }
        return trim(preg_replace('/ {2,}/', ' ', join(' ',$out)));
    }

}