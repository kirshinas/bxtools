<?php
/**
 * @author: Alexandr Kirshin <kirshin.as@gmail.com>
 */
namespace kirshinas\helpers;

use Exception;
use Imagine\Gd\Imagine;
use Imagine\Image\Box;
use Imagine\Image\Palette\RGB;
use Imagine\Image\Point;


class ImageHelper
{
    const THUMBNAIL_INSET = 'inset';
    const THUMBNAIL_OUTBOUND = 'outbound';

    const FLIP_HORIZONTALLY = 'Horizontally';
    const FLIP_VERTICALLY = 'Vertically';

    const POSITION_TOP_LEFT = 'tl';
    const POSITION_TOP_CENTER = 'tc';
    const POSITION_TOP_RIGHT = 'tr';
    const POSITION_MIDDLE_LEFT = 'ml';
    const POSITION_MIDDLE_CENTER = 'mc';
    const POSITION_MIDDLE_RIGHT = 'mr';
    const POSITION_BOTTOM_LEFT = 'bl';
    const POSITION_BOTTOM_CENTER = 'bc';
    const POSITION_BOTTOM_RIGHT = 'br';

    public static $cachePath;
    public static $rootPath;

    public $fileName;

    private $_manipulationStack = array();


    public static function open($fileName)
    {
        if (!self::$cachePath || !self::$rootPath) {
            throw new Exception('Не указана корневая директория и директория кеша');
        }
        if (!file_exists(self::$rootPath . $fileName)) {
            throw new Exception('Файл ' . $fileName . ' не найден.');
        }

        return new ImageHelper($fileName);
    }

    private function __construct($fileName)
    {
        $this->fileName = $fileName;
    }

    /**
     * Обрезать
     * @param $x
     * @param $y
     * @param $w
     * @param $h
     * @return $this
     */
    public function crop($x, $y, $w, $h)
    {
        $this->_manipulationStack["c_{$x}_{$y}_{$w}_{$h}"] = array('method' => 'crop', 'params' => array(new Point($x, $y), new Box($w, $h)));

        return $this;
    }

    /**
     * Ужать до размеров
     * @param $w
     * @param $h
     * @param string $mode
     * @return $this
     */
    public function thumbnail($w, $h, $mode = self::THUMBNAIL_INSET)
    {
        $this->_manipulationStack["t_{$w}_{$h}_{$mode}"] = array('method' => 'thumbnail', 'params' => array(new Box($w, $h), $mode));

        return $this;
    }

    /**
     * Ужать до ширины
     * @param $w
     * @return $this
     */
    public function widen($w)
    {
        $this->_manipulationStack["widen_{$w}"] = array('method' => 'widen', 'params' => array('w' => $w));

        return $this;
    }

    /**
     * Ужать до высоты
     * @param $h
     * @return $this
     */
    public function heighten($h)
    {
        $this->_manipulationStack["heighten_{$h}"] = array('method' => 'heighten', 'params' => array('h' => $h));

        return $this;
    }

    /**
     * Зеркально отразить
     * @param $mode
     * @return $this
     */
    public function flip($mode)
    {
        $this->_manipulationStack["f_{$mode}"] = array('method' => 'flip' . $mode, 'params' => array());

        return $this;
    }

    /**
     * Наложить водяной знак
     * @param $watermark
     * @param string $position
     * @return $this
     * @throws \Exception
     */
    public function watermark($watermark, $position = self::POSITION_BOTTOM_RIGHT)
    {
        if (!file_exists(self::$rootPath . $watermark)) {
            throw new Exception('Файл ' . $watermark . ' не найден.');
        }
        $this->_manipulationStack["f_w_{$position}"] = array('method' => 'watermark', 'params' => array('watermark' => $watermark, 'position' => $position));

        return $this;
    }

    /**
     * Преоброзовать в черно белую картинку
     * @return $this
     */
    public function grayscale()
    {
        $this->_manipulationStack["gs"] = array('method' => 'grayscale', 'params' => array());

        return $this;
    }

    /**
     * Изменить гамму
     * @param int $percent
     * @return $this
     */
    public function gamma($percent = 1)
    {
        $this->_manipulationStack["gm_{$percent}"] = array('method' => 'gamma', 'params' => array('percent' => $percent));

        return $this;
    }

    /**
     * Наложить полупрозрачный слой
     * @param $percent
     * @param string $color
     * @return $this
     */
    public function overlay($percent, $color = '#000000')
    {
        $this->_manipulationStack["ov_{$percent}"] = array('method' => 'overlay', 'params' => array('percent' => $percent, 'color' => $color));

        return $this;
    }

    /**
     * Сохранить изображение
     * @param string $fileName
     * @return string
     */
    public function save($fileName = '')
    {
        if (!$fileName) {
            $fileName = $this->generateFileName();
        }
        if (true || !$this->isCached($this->fileName, $fileName)) {
            $imagine = new Imagine();
            $image = $imagine->open(self::$rootPath . $this->fileName);

            foreach ($this->_manipulationStack as $action) {
                if ($action['method'] == 'watermark') {
                    $watermark = $imagine->open(self::$rootPath . $action['params']['watermark']);
                    $size = $image->getSize();
                    $wSize = $watermark->getSize();
                    switch ($action['params']['position']) {
                        case self::POSITION_TOP_LEFT:
                            $position = new Point(0, 0);
                            break;
                        case self::POSITION_TOP_CENTER:
                            $position = new Point($size->getWidth() / 2 - $wSize->getWidth() / 2, 0);
                            break;
                        case self::POSITION_TOP_RIGHT:
                            $position = new Point($size->getWidth() - $wSize->getWidth(), 0);
                            break;
                        case self::POSITION_MIDDLE_LEFT:
                            $position = new Point(0, $size->getHeight() / 2 - $wSize->getHeight() / 2);
                            break;
                        case self::POSITION_MIDDLE_CENTER:
                            $position = new Point($size->getWidth() / 2 - $wSize->getWidth() / 2, $size->getHeight() / 2 - $wSize->getHeight() / 2);
                            break;
                        case self::POSITION_MIDDLE_RIGHT:
                            $position = new Point($size->getWidth() - $wSize->getWidth(), $size->getHeight() / 2 - $wSize->getHeight() / 2);
                            break;
                        case self::POSITION_BOTTOM_LEFT:
                            $position = new Point(0, $size->getHeight() - $wSize->getHeight());
                            break;
                        case self::POSITION_BOTTOM_CENTER:
                            $position = new Point($size->getWidth() / 2 - $wSize->getWidth() / 2, $size->getHeight() - $wSize->getHeight());
                            break;
                        case self::POSITION_BOTTOM_RIGHT:
                            $position = new Point($size->getWidth() - $wSize->getWidth(), $size->getHeight() - $wSize->getHeight());
                            break;
                        default:
                            $position = new Point($size->getWidth() - $wSize->getWidth(), $size->getHeight() - $wSize->getHeight());
                            break;
                    }
                    $image->paste($watermark, $position);
                } elseif ($action['method'] == 'grayscale') {
                    $image->effects()->grayscale();
                } elseif ($action['method'] == 'gamma') {
                    $image->effects()->gamma($action['params']['percent']);
                } elseif ($action['method'] == 'overlay') {
                    $palette = new RGB();
                    $overlay = new Imagine();
                    $overlayLayer = $overlay->create($image->getSize(), $palette->color($action['params']['color'], $action['params']['percent']));
                    $image->paste($overlayLayer, new Point(0, 0));
                } elseif ($action['method'] == 'widen') {
                    $image = $image->thumbnail($image->getSize()->widen($action['params']['w']), self::THUMBNAIL_INSET);
                } elseif ($action['method'] == 'heighten') {
                    $image = $image->thumbnail($image->getSize()->heighten($action['params']['h']), self::THUMBNAIL_INSET);
                } else {
                    $image = call_user_func_array(array($image, $action['method']), $action['params']);
                }
            }
            $image->save(self::$rootPath . $fileName);
            touch(self::$rootPath . $fileName, filemtime(self::$rootPath . $this->fileName));
        }

        return $fileName;
    }

    private function generateFileName()
    {
        $pathInfo = pathinfo($this->fileName);
        $prefix = '';
        foreach ($this->_manipulationStack as $key => $action) {
            $prefix .= '_' . $key;
        }
        $prefix = ltrim($prefix, '_');
        $fileName = self::$cachePath . trim($pathInfo["dirname"], '/') . '/' . md5($prefix . "_" . $pathInfo["basename"]) . '.' . $pathInfo["extension"];
        $fullPath = self::$rootPath . dirname($fileName);
        if (!file_exists($fullPath)) {
            mkdir($fullPath, 0777, true);
        }

        return $fileName;
    }

    private function isCached($sourceFile, $destinationFile)
    {
        if (!file_exists(self::$rootPath . $destinationFile)) {
            return false;
        }
        if (file_exists(self::$rootPath . $sourceFile)) {
            $sourceTime = filemtime(self::$rootPath . $sourceFile);
            $destinationTime = filemtime(self::$rootPath . $destinationFile);

            return $sourceTime == $destinationTime;
        }

        return true;
    }
}