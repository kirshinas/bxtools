<?php
/**
 * @author: Alexandr Kirshin <kirshin.as@gmail.com>
 */

namespace kirshinas\widgets;


/**
 * Вспомогательный класс для формирования ссылок для установки количества элементов на странице
 * Class Paging
 * @package victory\widgets
 */
class Pager
{

    private $_countParamName;
    private $_count;
    private $_fields;

    public function __construct($countParamName, $defCount)
    {
        $this->_countParamName = $countParamName;
        $this->_count = $_GET[$countParamName] ? $_GET[$countParamName] : $defCount;
    }

    public function add($id, $label)
    {
        global $APPLICATION;
        $this->_fields[$id] = array(
            'label'  => $label,
            'active' => $id == $this->_count,
            'url'   => $APPLICATION->GetCurPageParam($this->_countParamName . '=' . $id, array($this->_countParamName))
        );
    }

    public function getFields()
    {
        return $this->_fields;
    }

    public function getCount()
    {
        return $this->_count;
    }


}