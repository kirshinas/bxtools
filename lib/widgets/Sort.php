<?php
/**
 * @author: Alexandr Kirshin <kirshin.as@gmail.com>
 */

namespace kirshinas\widgets;

/**
 * Вспомогательный класс для формирования ссылок для сортировки
 * Class Sort
 * @package Victory\Widgets
 */
class Sort
{

    private $_sort;
    private $_order;
    private $_sortParamName;
    private $_orderParamName;
    private $_fields;

    public function __construct($sortParamName, $orderParamName, $defField, $defOrder)
    {
        $this->_sortParamName = $sortParamName;
        $this->_orderParamName = $orderParamName;
        $this->_sort = $_REQUEST[$sortParamName] ? $_REQUEST[$sortParamName] : $defField;
        $this->_order = $_REQUEST[$orderParamName] ? $_REQUEST[$orderParamName] : $defOrder;
    }

    public function add($id, $field, $label, $up = 'up', $down = 'down')
    {
        global $APPLICATION;
        $this->_fields[$id] = array(
            'id'  => $id,
            'field'  => $field,
            'label'  => $label,
            'active' => $id == $this->_sort,
            'url'    => $APPLICATION->GetCurPageParam($this->_sortParamName . '=' . $id . '&' . $this->_orderParamName . '=' . ($this->_order == 'asc' ? 'desc' : 'asc'), array($this->_sortParamName, $this->_orderParamName)),
            'order'  => $this->_order == 'asc' ? $up : $down
        );
    }

    public function getFields()
    {
        return $this->_fields;
    }

    public function getField()
    {
        return $this->_fields[$this->_sort]['field'];
    }

    public function getOrder()
    {
        return $this->_order;
    }

    public function getCurrentField()
    {
        return $this->_fields[$this->_sort];
    }

    public function getUrl($field, $order)
    {
        global $APPLICATION;
        return $APPLICATION->GetCurPageParam($this->_sortParamName . '=' . $field['id'] . '&' . $this->_orderParamName . '=' . ($order), array($this->_sortParamName, $this->_orderParamName));
    }
}